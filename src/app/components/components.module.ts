import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostComponent } from './post/post.component';
import { PostsComponent } from './posts/posts.component';
import { IonicModule } from '@ionic/angular';
import { PostDetailsComponent } from './post-details/post-details.component';
import { CommentsPageComponent } from './comments-page/comments-page.component';
import { CommentsComponent } from './comments/comments.component';
import { CommentComponent } from './comment/comment.component';
import { NavbarComponent } from './shared-components/navbar/navbar.component';



@NgModule({
  entryComponents:[PostDetailsComponent,CommentsPageComponent],
  declarations: [PostComponent,PostsComponent,PostDetailsComponent,CommentsPageComponent,CommentsComponent,CommentComponent,NavbarComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports:[PostComponent,PostsComponent,NavbarComponent]
})
export class ComponentsModule { }
