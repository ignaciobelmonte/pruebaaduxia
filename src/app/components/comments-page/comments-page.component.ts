import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Post, Comment } from '../../interfaces/interfaces';
import { PostService } from '../../services/post.service';

@Component({
  selector: 'app-comments-page',
  templateUrl: './comments-page.component.html',
  styleUrls: ['./comments-page.component.scss'],
})
export class CommentsPageComponent implements OnInit {
  @Input() post: Post;
  loading=false;
  comments:Comment[];
  constructor( 
    private modalController:ModalController,
    private postService:PostService
    ) { }

  ngOnInit() {
    this.loading=true;
    this.postService.getCommetPost(this.post.id).subscribe((data:Comment[])=>{
      this.comments=data||[];
      this.loading=false;
    })
  }

  dismiss(){
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
