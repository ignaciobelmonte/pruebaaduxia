import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { PostDetailsComponent } from '../post-details/post-details.component';
import { CommentsPageComponent } from '../comments-page/comments-page.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {

  @Input() post: Post;

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {}

  async openPost(){
    const modal = await this.modalController.create({
      component: PostDetailsComponent,
      componentProps: {
        post: this.post
      }
    });
    modal.present();
  }

  async showComments(){
    const modal = await this.modalController.create({
      component: CommentsPageComponent,
      componentProps: {
        post: this.post
      }
    });
    modal.present();
  }
}
