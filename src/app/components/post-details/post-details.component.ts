import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { PostService } from '../../services/post.service';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss'],
})
export class PostDetailsComponent implements OnInit {

  @Input() post: Post;
  constructor(
    private modalController:ModalController,
    private PostService:PostService
  ) { }

  ngOnInit() {
    console.log(this.post)
  }

  dismiss(){
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
