import { Component } from '@angular/core';
import { PostService } from '../services/post.service';
import { Post } from '../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { PostDetailsComponent } from '../components/post-details/post-details.component';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  loading=false;
  posts:Post[]=[]

  constructor(
    private postService:PostService,
    private modalController:ModalController
  ) {}

  ngOnInit() {
    this.loadPost();
  }
  async openPost(post:Post){
    const modal = await this.modalController.create({
      component: PostDetailsComponent,
      componentProps: {
        post: post
      }
    });
    modal.present();
  }

  loadPost(){
    this.loading=true;
    this.postService.getPost().subscribe((data:Post[])=>{
      this.posts=data|| [];
      this.loading=false;
    })
  }
}
