import { Component, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  @ViewChild("slideMain", { static: true }) slidesMain: IonSlides;


  constructor() { }

  async ngOnInit() {
    await this.slidesMain.lockSwipes(true);
  }


  async mostrarRegistro() {
    await this.slidesMain.lockSwipes(false);
    await this.slidesMain.slideTo(1);
    await this.slidesMain.lockSwipes(true);
  }

  async mostarIngresar() {
    await this.slidesMain.lockSwipes(false);
    await this.slidesMain.slideTo(0);
    await this.slidesMain.lockSwipes(true);
  }

}
