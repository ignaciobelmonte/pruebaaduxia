import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';
import { Post } from '../interfaces/interfaces';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  loading=false;
  posts:Post[]=[]

  constructor(
    private postService:PostService
  ) {}

  ngOnInit() {
    this.loadPost();
  }

  loadPost(){
    this.loading=true;
    this.postService.getPost().subscribe((data:Post[])=>{
      this.posts=data|| [];
      this.loading=false;
    })
  }

}
