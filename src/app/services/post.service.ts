import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Post } from '../interfaces/interfaces';
const url = environment.url

@Injectable({
  providedIn: 'root'
})


export class PostService {

  constructor(private http: HttpClient) { }


  getPost(){
    return this.http.get(`${url}/posts`);
  }

  getCommetPost(id:number){
    return this.http.get(`${url}/posts/${id}/comments`);
  }

}
